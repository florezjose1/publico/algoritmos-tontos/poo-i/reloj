/**
 * Un ejemplo que modela un Reloj simple usando POO
 *
 * @author Ejercicio: (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)

 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Reloj {

    protected int segundos;
    protected int minutos;
    protected int horas;
    protected boolean is_am;

    public Reloj() {

    }

    public Reloj(int segundos, int minutos, int horas) {
        this.segundos = segundos;
        this.minutos = minutos;
        this.horas = horas;
        this.is_am = true;
    }

    /**
     * Metodo de acceso a la propiedad segundos
     */
    public int getSegundos() {
        return this.segundos;
    }//end method getSegundos

    /**
     * Metodo de modificación a la propiedad segundos
     */
    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }//end method setSegundos

    /**
     * Metodo de acceso a la propiedad minutos
     */
    public int getMinutos() {
        return this.minutos;
    }//end method getMinutos

    /**
     * Metodo de modificación a la propiedad minutos
     */
    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }//end method setMinutos

    /**
     * Metodo de acceso a la propiedad horas
     */
    public int getHoras() {
        return this.horas;
    }//end method getHoras

    /**
     * Metodo de modificación a la propiedad horas
     */
    public void setHoras(int horas) {
        this.horas = horas;
    }//end method setHoras

    /*De aqui en adelante Ud. debe completar los algoritmos para que el programa funcione*/
    /**
     * Metodo para mover le segundero
     */
    public void moverSegundero() {
        //COMPLETE EL ALGORITMO
        if (this.segundos == 59) {
            this.segundos = 0;
            this.moverMinutero();
        } else {
            this.segundos++;
        }

    }

    /**
     * Metodo para mover le minutero
     */
    public void moverMinutero() {
        //COMPLETE EL ALGORITMO
        if (this.minutos == 59) {
            this.minutos = 0;
            this.moverHorario();
        } else {
            this.minutos++;
        }
    }

    /**
     * Metodo para mover le horario
     */
    public void moverHorario() {
        //COMPLETE EL ALGORITMO
        if (this.horas == 12) {
            this.horas = 1;
        } else {
            this.horas++;
        }
    }
}

