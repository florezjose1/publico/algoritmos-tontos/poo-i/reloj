### El Reloj

Se requiere un programa para simular un reloj:


1. Es un reloj bastante simple y anticuado ;)
2. El reloj permite mover el horario de 1 a 12 y regresar de 12 a 1, circularmente.
3. El reloj permite mover el minutero. Cuando el minutero llega a 60 minutos, mueve el horario una unidad y reinicia el minutero a cero.
4. El reloj permite mover el segundero. Cuando el segundero llega a 60 segundos, mueve el minutero una unidad y reinicia el segundero a cero.

Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
